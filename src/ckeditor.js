/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

// The editor creator to use.
import BalloonEditorBase from '@ckeditor/ckeditor5-editor-balloon/src/ballooneditor';

import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import UploadAdapter from '@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter';
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
// import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
import CKFinder from '@ckeditor/ckeditor5-ckfinder/src/ckfinder';
import EasyImage from '@ckeditor/ckeditor5-easy-image/src/easyimage';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Image from '@ckeditor/ckeditor5-image/src/image';
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload';
import Indent from '@ckeditor/ckeditor5-indent/src/indent';
import Link from '@ckeditor/ckeditor5-link/src/link';
import List from '@ckeditor/ckeditor5-list/src/list';
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice';
import Table from '@ckeditor/ckeditor5-table/src/table';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
import Adapter from './adapter';
import BlockToolbar from '@ckeditor/ckeditor5-ui/src/toolbar/block/blocktoolbar';
import HeadingButtonsUI from '@ckeditor/ckeditor5-heading/src/headingbuttonsui';
import ParagraphButtonUI from '@ckeditor/ckeditor5-paragraph/src/paragraphbuttonui';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
import RemoveFormat from '@ckeditor/ckeditor5-remove-format/src/removeformat';
import CodeBlock from '@ckeditor/ckeditor5-code-block/src/codeblock';

import '../theme/custom.css';
import { languages } from './config/codeblocks.config';

/**
 * Upload adapter plugin, calls callback of config.uploadHandler
 * that must return a string containing a link to the uploaded image.
 */
function UploadAdapterPlugin( editor ) {
	editor.plugins.get( 'FileRepository' ).createUploadAdapter = loader => {
		return new Adapter( loader, MindsEditor.config.uploadHandler );
	};
}

/**
 * Theme switcher plugin
 * Selects one of two themes, light or dark
 * An Observable object must be passed to allow dynamic switching.
 */
function ThemeSwitcherPlugin() {
	try {
		if ( !MindsEditor.config || !MindsEditor.config.isDark$ ) {
			console.warn( 'MindsEditor | You have not properly configured the ThemeSwitcher plugin' );
			return;
		}
		MindsEditor.config.isDark$.subscribe( darkMode => {
			// console.log( `MindsEditor | theme switched to ${ darkMode ? 'dark mode' : 'light mode' }` );
			if ( darkMode ) {
				document.documentElement.setAttribute( 'id', 'light' );
				return;
			}
			document.documentElement.setAttribute( 'id', null );
		} );
	} catch ( e ) {
		console.error( e );
	}
}

export default class MindsEditor extends BalloonEditorBase {}

// Plugins to include in the build.
MindsEditor.builtinPlugins = [
	Essentials,
	UploadAdapter,
	Autoformat,
	Bold,
	Italic,
	// BlockQuote,
	CKFinder,
	EasyImage,
	Heading,
	Image,
	ImageCaption,
	ImageStyle,
	ImageToolbar,
	ImageUpload,
	Indent,
	Link,
	List,
	MediaEmbed,
	Paragraph,
	PasteFromOffice,
	Table,
	TableToolbar,
	Alignment,
	BlockToolbar,
	HeadingButtonsUI,
	ParagraphButtonUI,
	Underline,
	Strikethrough,
	RemoveFormat,
	CodeBlock
];

// Editor configuration.
MindsEditor.defaultConfig = {
	placeholder: 'Speak your mind...',
	toolbar: {
		items: [
			'heading',
			'|',
			'bold',
			'italic',
			'underline',
			'strikethrough',
			'bulletedList',
			'numberedList',
			'removeFormat',
			'|',
			'alignment',
			'link',
			// 'blockQuote',
			'insertTable',
			'codeBlock'
		]
	},
	blockToolbar: [
		'imageUpload',
		'mediaEmbed'
	],
	image: {
		toolbar: [
			// 'imageStyle:full',
			// 'imageStyle:side',
			// '|',
			'imageTextAlternative'
		]
	},
	mediaEmbed: {
		previewsInData: true
	},
	heading: {
		options: [
			{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
			{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
			{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
			{ model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' }
		]
	},
	table: {
		contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
	},
	codeBlock: { languages },
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: 'en',
	extraAllowedContent: '*(*)',
	allowedContent: true,
	previewsInData: false,
	extraPlugins: [ UploadAdapterPlugin, ThemeSwitcherPlugin ]
};
